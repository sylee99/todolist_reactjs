import React, {Component} from "react";
import './TodolList.css'
import classNames from 'classnames';
class TodoList extends Component{

    // constructor(prors) {
    //     super(prors);
    //     // this.onItemClick = this.onItemClick.bind(this);
    // }

    // onItemClick(){
    //     console.log(this.props.item)
    // }
    render() {
        const {item} = this.props
        return (
            <div>
            <ul className="Ul">
                {/*<div onClick={() => this.onItemClick()} className={classNames({'clickItem' : item.isComplete})}>{item.tittle}</div>*/}
                <div  className={classNames({'clickItem' : item.isComplete})}  onClick={this.props.onClicks}> {item.tittle}</div>
            </ul>
            </div>
        );
    }
}

export default TodoList;